import { MessagePublisherPort } from '@mobicoop/ddd-library';
import { ModuleMetadata } from '@nestjs/common';
import { HealthRepositoryPort } from '../../application/ports/health-repository.port';
import { Transport } from '@nestjs/microservices';

export interface HealthModuleOptions {
  serviceName: string;
  checkUrls?: CheckUrl[];
  checkRepositories?: CheckRepository[];
  checkMicroservices?: CheckMicroservice[];
  checkGrpcs?: CheckGrpc[];
  messagePublisher?: MessagePublisherPort;
  criticalLoggingKey: string;
}

export interface HealthModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  useFactory: (
    ...args: any[]
  ) => HealthModuleOptions | Promise<HealthModuleOptions>;
  inject?: any[];
}

export type CheckUrl = {
  name: string;
  url: string;
};

export type CheckRepository = {
  name: string;
  repository: HealthRepositoryPort;
};

export type CheckMicroservice = {
  name: string;
  host: string;
  port: string;
  transport: Transport;
  password?: string;
};

export type CheckGrpc = {
  name: string;
  url: string;
  port: string;
};
