import { Injectable } from '@nestjs/common';
import {
  HealthCheckError,
  HealthIndicator,
  HealthIndicatorResult,
} from '@nestjs/terminus';
import { CheckRepository } from '../../domain/types/health.types';
import { DatabaseErrorException } from '@mobicoop/ddd-library';

@Injectable()
export class RepositoryHealthIndicator extends HealthIndicator {
  isHealthy = async (
    checkRepository: CheckRepository,
  ): Promise<HealthIndicatorResult> => {
    try {
      const isHealthy = await checkRepository.repository.healthCheck();
      const result = this.getStatus(checkRepository.name, isHealthy);
      if (isHealthy) return result;
      throw new HealthCheckError(
        `Repository check for ${checkRepository.name} failed`,
        result,
      );
    } catch (e: any) {
      if (e instanceof DatabaseErrorException) {
        throw new HealthCheckError(
          `Repository check for ${checkRepository.name} failed`,
          e.message,
        );
      }
      throw e;
    }
  };
}
