export interface HealthRepositoryPort {
  healthCheck(): Promise<boolean>;
}
