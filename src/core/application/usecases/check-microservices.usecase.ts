import { Injectable } from '@nestjs/common';
import {
  HealthIndicatorFunction,
  MicroserviceHealthIndicator,
} from '@nestjs/terminus';
import { CheckMicroservice } from '../../domain/types/health.types';

@Injectable()
export class CheckMicroservicesUseCase {
  constructor(
    private readonly microserviceHealthIndicator: MicroserviceHealthIndicator,
  ) {}
  results = (
    checkMicroservices: CheckMicroservice[],
  ): HealthIndicatorFunction[] =>
    checkMicroservices?.map(
      (checkMicroservice: CheckMicroservice) => () =>
        this.microserviceHealthIndicator.pingCheck(checkMicroservice.name, {
          transport: checkMicroservice.transport,
          options: {
            host: checkMicroservice.host,
            port: checkMicroservice.port,
            password: checkMicroservice.password,
          },
        }),
    );
}
