import { Injectable } from '@nestjs/common';
import { GRPCHealthIndicator, HealthIndicatorFunction } from '@nestjs/terminus';
import { CheckGrpc } from '../../domain/types/health.types';
import { GrpcOptions } from '@nestjs/microservices';
import { join } from 'path';

const SERVICE = 'health';
const PACKAGE = 'health';
const TIMEOUT = 1000;
const PROTO = '../../../../src/health.proto';

@Injectable()
export class CheckGrpcsUseCase {
  constructor(private readonly grpcHealthIndicator: GRPCHealthIndicator) {}
  results = (checkGrpcs: CheckGrpc[]): HealthIndicatorFunction[] =>
    checkGrpcs?.map(
      (checkGrpc: CheckGrpc) => () =>
        this.grpcHealthIndicator.checkService<GrpcOptions>(
          checkGrpc.name,
          SERVICE,
          {
            timeout: TIMEOUT,
            package: PACKAGE,
            protoPath: join(__dirname, PROTO),
            url: `${checkGrpc.url}:${checkGrpc.port}`,
          },
        ),
    );
}
