import { Injectable } from '@nestjs/common';
import { CheckRepository } from '../../domain/types/health.types';
import { HealthIndicatorFunction } from '@nestjs/terminus';
import { RepositoryHealthIndicator } from '../health-indicators/repository.health-indicator';

@Injectable()
export class CheckRepositoriesUseCase {
  constructor(
    private readonly repositoryHealthIndicator: RepositoryHealthIndicator,
  ) {}
  results = (checkRepositories: CheckRepository[]): HealthIndicatorFunction[] =>
    checkRepositories?.map(
      (checkRepository: CheckRepository) => () =>
        this.repositoryHealthIndicator.isHealthy(checkRepository),
    );
}
