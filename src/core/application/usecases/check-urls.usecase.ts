import { Injectable } from '@nestjs/common';
import { HealthIndicatorFunction, HttpHealthIndicator } from '@nestjs/terminus';
import { CheckUrl } from '../../domain/types/health.types';

@Injectable()
export class CheckUrlsUseCase {
  constructor(private readonly httpHealthIndicator: HttpHealthIndicator) {}
  results = (checkUrls: CheckUrl[]): HealthIndicatorFunction[] =>
    checkUrls?.map(
      (checkUrl: CheckUrl) => () =>
        this.httpHealthIndicator.pingCheck(checkUrl.name, checkUrl.url),
    );
}
