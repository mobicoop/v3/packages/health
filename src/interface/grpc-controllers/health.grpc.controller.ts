import {
  Controller,
  Inject,
  ServiceUnavailableException,
} from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import {
  HealthCheckResult,
  HealthCheckService,
  HealthIndicatorFunction,
} from '@nestjs/terminus';
import { CheckRepositoriesUseCase } from '../../core/application/usecases/check-repositories.usecase';
import { CheckUrlsUseCase } from '../../core/application/usecases/check-urls.usecase';
import {
  CHECK_GRPCS,
  CHECK_MICROSERVICES,
  CHECK_REPOSITORIES,
  CHECK_URLS,
  CRITICAL_LOGGING_KEY,
  HEALTH_MESSAGE_PUBLISHER,
} from '../../health.di-tokens';
import {
  CheckGrpc,
  CheckMicroservice,
  CheckRepository,
  CheckUrl,
} from '../../core/domain/types/health.types';
import { MessagePublisherPort } from '@mobicoop/ddd-library';
import { CheckMicroservicesUseCase } from '../../core/application/usecases/check-microservices.usecase';
import { CheckGrpcsUseCase } from '../../core/application/usecases/check-grpcs.usecase';

export enum ServingStatus {
  UNKNOWN = 0,
  SERVING = 1,
  NOT_SERVING = 2,
}

interface HealthCheckRequest {
  service: string;
}

interface HealthCheckResponse {
  status: ServingStatus;
  message?: string;
}

@Controller()
export class HealthGrpcController {
  private readonly urlsHealthIndicatorFunctions: HealthIndicatorFunction[];
  private readonly repositoriesHealthIndicatorFunctions: HealthIndicatorFunction[];
  private readonly microservicesHealthIndicatorFunctions: HealthIndicatorFunction[];
  private readonly grpcsHealthIndicatorFunctions: HealthIndicatorFunction[];

  constructor(
    @Inject(CHECK_URLS)
    private readonly checkUrls: CheckUrl[],
    @Inject(CHECK_REPOSITORIES)
    private readonly checkRepositories: CheckRepository[],
    @Inject(CHECK_MICROSERVICES)
    private readonly checkMicroservices: CheckMicroservice[],
    @Inject(CHECK_GRPCS)
    private readonly checkGrpcs: CheckGrpc[],
    @Inject(HEALTH_MESSAGE_PUBLISHER)
    private readonly messagePublisher: MessagePublisherPort,
    @Inject(CRITICAL_LOGGING_KEY)
    private readonly criticalLoggingKey: string,
    private readonly checkUrlsUseCase: CheckUrlsUseCase,
    private readonly checkRepositoriesUseCase: CheckRepositoriesUseCase,
    private readonly checkMicroservicesUseCase: CheckMicroservicesUseCase,
    private readonly checkGrpcsUseCase: CheckGrpcsUseCase,
    private readonly healthCheckService: HealthCheckService,
  ) {
    this.urlsHealthIndicatorFunctions =
      checkUrlsUseCase.results(checkUrls) ?? [];
    this.repositoriesHealthIndicatorFunctions =
      checkRepositoriesUseCase.results(checkRepositories) ?? [];
    this.microservicesHealthIndicatorFunctions =
      checkMicroservicesUseCase.results(checkMicroservices) ?? [];
    this.grpcsHealthIndicatorFunctions =
      checkGrpcsUseCase.results(checkGrpcs) ?? [];
  }

  @GrpcMethod('Health', 'Check')
  async check(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    data?: HealthCheckRequest,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    metadata?: any,
  ): Promise<HealthCheckResponse> {
    try {
      const healthCheckResult: HealthCheckResult =
        await this.healthCheckService.check([
          ...this.repositoriesHealthIndicatorFunctions,
          ...this.urlsHealthIndicatorFunctions,
          ...this.microservicesHealthIndicatorFunctions,
          ...this.grpcsHealthIndicatorFunctions,
        ]);
      return {
        status:
          healthCheckResult.status == 'ok'
            ? ServingStatus.SERVING
            : ServingStatus.NOT_SERVING,
      };
    } catch (error: any) {
      let message = 'An error occured';
      if (error instanceof ServiceUnavailableException) {
        message = JSON.stringify(error.getResponse());
      }
      if (this.messagePublisher)
        this.messagePublisher.publish(this.criticalLoggingKey, message);
      return {
        status: ServingStatus.NOT_SERVING,
        message,
      };
    }
  }
}
