import { Test, TestingModule } from '@nestjs/testing';
import { CheckRepositoriesUseCase } from '../../core/application/usecases/check-repositories.usecase';
import { RepositoryHealthIndicator } from '../../core/application/health-indicators/repository.health-indicator';
import { CheckRepository } from '../../core/domain/types/health.types';
import { HealthIndicatorFunction } from '@nestjs/terminus';

const mockRepositoryHealthIndicator = {
  isHealthy: jest.fn().mockImplementation((checkRepository: CheckRepository) =>
    Promise.resolve({
      [checkRepository.name]: {
        status: 'up',
      },
    }),
  ),
};

const mockFirstRepository = {
  healthCheck: jest.fn().mockImplementation(() => {
    return Promise.resolve(true);
  }),
};

const mockSecondRepository = {
  healthCheck: jest.fn().mockImplementation(() => {
    return Promise.resolve(true);
  }),
};

const checkRepositories: CheckRepository[] = [
  {
    name: 'firstRepository',
    repository: mockFirstRepository,
  },
  {
    name: 'secondRepository',
    repository: mockSecondRepository,
  },
];

describe('CheckRepositoriesUseCase', () => {
  let checkRepositoriesUseCase: CheckRepositoriesUseCase;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: RepositoryHealthIndicator,
          useValue: mockRepositoryHealthIndicator,
        },
        CheckRepositoriesUseCase,
      ],
    }).compile();

    checkRepositoriesUseCase = module.get<CheckRepositoriesUseCase>(
      CheckRepositoriesUseCase,
    );
  });

  it('should be defined', () => {
    expect(checkRepositoriesUseCase).toBeDefined();
  });

  it('should return results as HealthIndicatorFunctions', () => {
    const results: HealthIndicatorFunction[] =
      checkRepositoriesUseCase.results(checkRepositories);
    expect(results.length).toBe(2);
    results.forEach((result: HealthIndicatorFunction) => {
      expect(result()).toBeInstanceOf(Promise);
    });
  });

  it('should return an empty results array if no repositories are given', () => {
    const emptyResults: HealthIndicatorFunction[] =
      checkRepositoriesUseCase.results([]);
    expect(emptyResults.length).toBe(0);
    const emptyFunctionArray: HealthIndicatorFunction[] =
      checkRepositoriesUseCase.results([]);
    expect(emptyFunctionArray).toHaveLength(0);
  });
});
