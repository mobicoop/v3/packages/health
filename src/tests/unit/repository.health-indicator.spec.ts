import { RepositoryHealthIndicator } from '../../core/application/health-indicators/repository.health-indicator';
import { DatabaseErrorException } from '@mobicoop/ddd-library';
import { HealthCheckError, HealthIndicatorResult } from '@nestjs/terminus';

const mockRepository = {
  healthCheck: jest
    .fn()
    .mockImplementationOnce(() => {
      return Promise.resolve(true);
    })
    .mockImplementationOnce(() => {
      return Promise.resolve(false);
    })
    .mockImplementation(() => {
      throw new DatabaseErrorException('An error occured in the database');
    }),
};

describe('RepositoryHealthIndicator', () => {
  it('should be defined', () => {
    expect(new RepositoryHealthIndicator()).toBeDefined();
  });

  it('should return a healthy result if repository is healthy', async () => {
    const repositoryHealthIndicator: RepositoryHealthIndicator =
      new RepositoryHealthIndicator();
    const healthIndicatorResult: HealthIndicatorResult =
      await repositoryHealthIndicator.isHealthy({
        name: 'fakeRepository',
        repository: mockRepository,
      });
    expect(healthIndicatorResult['fakeRepository'].status).toBe('up');
  });

  it('should throw a HealthCheckError if repository is unhealthy', async () => {
    const repositoryHealthIndicator: RepositoryHealthIndicator =
      new RepositoryHealthIndicator();
    await expect(
      repositoryHealthIndicator.isHealthy({
        name: 'fakeRepository',
        repository: mockRepository,
      }),
    ).rejects.toBeInstanceOf(HealthCheckError);
  });

  it('should throw a HealthCheckError if a DatabaseErrorException occurs', async () => {
    const repositoryHealthIndicator: RepositoryHealthIndicator =
      new RepositoryHealthIndicator();
    await expect(
      repositoryHealthIndicator.isHealthy({
        name: 'fakeRepository',
        repository: mockRepository,
      }),
    ).rejects.toBeInstanceOf(HealthCheckError);
  });
});
