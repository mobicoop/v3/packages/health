import { Test, TestingModule } from '@nestjs/testing';
import {
  HealthGrpcController,
  ServingStatus,
} from '../../interface/grpc-controllers/health.grpc.controller';
import {
  CHECK_GRPCS,
  CHECK_MICROSERVICES,
  CHECK_REPOSITORIES,
  CHECK_URLS,
  CRITICAL_LOGGING_KEY,
  HEALTH_MESSAGE_PUBLISHER,
} from '../../health.di-tokens';
import {
  CheckGrpc,
  CheckMicroservice,
  CheckRepository,
  CheckUrl,
} from '../../core/domain/types/health.types';
import { CheckUrlsUseCase } from '../../core/application/usecases/check-urls.usecase';
import { CheckRepositoriesUseCase } from '../../core/application/usecases/check-repositories.usecase';
import { HealthCheckService } from '@nestjs/terminus';
import { ServiceUnavailableException } from '@nestjs/common';
import { CheckMicroservicesUseCase } from '../../core/application/usecases/check-microservices.usecase';
import { CheckGrpcsUseCase } from '../../core/application/usecases/check-grpcs.usecase';
import { Transport } from '@nestjs/microservices';

const checkUrls: CheckUrl[] = [
  {
    name: 'firstUrl',
    url: 'https://www.firsturl.com',
  },
  {
    name: 'secondUrl',
    url: 'https://www.secondurl.com',
  },
];

const mockFirstRepository = {
  healthCheck: jest.fn().mockImplementation(() => {
    return Promise.resolve(true);
  }),
};

const mockSecondRepository = {
  healthCheck: jest.fn().mockImplementation(() => {
    return Promise.resolve(true);
  }),
};

const checkRepositories: CheckRepository[] = [
  {
    name: 'firstRepository',
    repository: mockFirstRepository,
  },
  {
    name: 'secondRepository',
    repository: mockSecondRepository,
  },
];

const checkMicroservices: CheckMicroservice[] = [
  {
    name: 'firstMicroservice',
    transport: Transport.TCP,
    host: 'microservice1.host',
    port: '5001',
  },
];

const checkGrpcs: CheckGrpc[] = [
  {
    name: 'firstGrpc',
    url: 'grpc.url',
    port: '5002',
  },
];

const mockPublisher = {
  publish: jest.fn(),
};

const mockCheckUrlsUseCase = {
  results: jest.fn(),
};

const mockCheckRepositoriesUseCase = {
  results: jest.fn(),
};

const mockCheckMicroservicesUseCase = {
  results: jest.fn(),
};

const mockCheckGrpcsUseCase = {
  results: jest.fn(),
};

const mockHealthCheckService = {
  check: jest
    .fn()
    .mockImplementationOnce(() => ({
      status: 'ok',
    }))
    .mockImplementationOnce(() => ({
      status: 'error',
    }))
    .mockImplementationOnce(() => {
      throw new ServiceUnavailableException({
        response: {
          message: 'Some Error Happened !',
        },
      });
    })
    .mockImplementationOnce(() => {
      throw new Error();
    }),
};

describe('Health Grpc Controller', () => {
  let healthGrpcController: HealthGrpcController;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: CHECK_URLS,
          useValue: checkUrls,
        },
        {
          provide: CHECK_REPOSITORIES,
          useValue: checkRepositories,
        },
        {
          provide: CHECK_MICROSERVICES,
          useValue: checkMicroservices,
        },
        {
          provide: CHECK_GRPCS,
          useValue: checkGrpcs,
        },
        {
          provide: HEALTH_MESSAGE_PUBLISHER,
          useValue: mockPublisher,
        },
        {
          provide: CRITICAL_LOGGING_KEY,
          useValue: 'criticalLoggingKey',
        },
        {
          provide: CheckUrlsUseCase,
          useValue: mockCheckUrlsUseCase,
        },
        {
          provide: CheckRepositoriesUseCase,
          useValue: mockCheckRepositoriesUseCase,
        },
        {
          provide: CheckMicroservicesUseCase,
          useValue: mockCheckMicroservicesUseCase,
        },
        {
          provide: CheckGrpcsUseCase,
          useValue: mockCheckGrpcsUseCase,
        },
        {
          provide: HealthCheckService,
          useValue: mockHealthCheckService,
        },
        HealthGrpcController,
      ],
    }).compile();

    healthGrpcController =
      module.get<HealthGrpcController>(HealthGrpcController);
  });

  afterEach(async () => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(healthGrpcController).toBeDefined();
  });

  it('should return a Serving status ', async () => {
    const servingStatus: { status: ServingStatus } =
      await healthGrpcController.check();
    expect(servingStatus).toEqual({
      status: ServingStatus.SERVING,
    });
  });

  it('should return a Not Serving status ', async () => {
    const servingStatus: { status: ServingStatus } =
      await healthGrpcController.check();
    expect(servingStatus).toEqual({
      status: ServingStatus.NOT_SERVING,
    });
  });

  it('should return a Not Serving status if an identified error occurs ', async () => {
    jest.spyOn(mockPublisher, 'publish');
    const servingStatus: { status: ServingStatus } =
      await healthGrpcController.check();
    expect(servingStatus).toEqual({
      status: ServingStatus.NOT_SERVING,
      message: '{"response":{"message":"Some Error Happened !"}}',
    });
    expect(mockPublisher.publish).toHaveBeenCalledTimes(1);
  });

  it('should return a Not Serving status if any other error occurs ', async () => {
    jest.spyOn(mockPublisher, 'publish');
    const servingStatus: { status: ServingStatus } =
      await healthGrpcController.check();
    expect(servingStatus).toEqual({
      status: ServingStatus.NOT_SERVING,
      message: 'An error occured',
    });
    expect(mockPublisher.publish).toHaveBeenCalledTimes(1);
  });
});
