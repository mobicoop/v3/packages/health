import { Test, TestingModule } from '@nestjs/testing';
import { CheckUrl } from '../../core/domain/types/health.types';
import { HealthIndicatorFunction, HttpHealthIndicator } from '@nestjs/terminus';
import { CheckUrlsUseCase } from '../../core/application/usecases/check-urls.usecase';

const mockHttpHealhIndicator = {
  pingCheck: jest.fn().mockImplementation((checkUrl: CheckUrl) =>
    Promise.resolve({
      [checkUrl.name]: {
        status: 'up',
      },
    }),
  ),
};

const checkUrls: CheckUrl[] = [
  {
    name: 'firstUrl',
    url: 'https://www.firsturl.com',
  },
  {
    name: 'secondUrl',
    url: 'https://www.secondurl.com',
  },
];

describe('CheckUrlsUseCase', () => {
  let checkUrlsUseCase: CheckUrlsUseCase;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: HttpHealthIndicator,
          useValue: mockHttpHealhIndicator,
        },
        CheckUrlsUseCase,
      ],
    }).compile();

    checkUrlsUseCase = module.get<CheckUrlsUseCase>(CheckUrlsUseCase);
  });

  it('should be defined', () => {
    expect(checkUrlsUseCase).toBeDefined();
  });

  it('should return results as HealthIndicatorFunctions', () => {
    const results: HealthIndicatorFunction[] =
      checkUrlsUseCase.results(checkUrls);
    expect(results.length).toBe(2);
    results.forEach((result: HealthIndicatorFunction) => {
      expect(result()).toBeInstanceOf(Promise);
    });
  });

  it('should return an empty results array if no urls are given', () => {
    const emptyResults: HealthIndicatorFunction[] = checkUrlsUseCase.results(
      [],
    );
    expect(emptyResults.length).toBe(0);
    const emptyFunctionArray: HealthIndicatorFunction[] =
      checkUrlsUseCase.results([]);
    expect(emptyFunctionArray).toHaveLength(0);
  });
});
