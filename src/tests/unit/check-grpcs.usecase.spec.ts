import { Test, TestingModule } from '@nestjs/testing';
import { CheckGrpc } from '../../core/domain/types/health.types';
import { GRPCHealthIndicator, HealthIndicatorFunction } from '@nestjs/terminus';
import { CheckGrpcsUseCase } from '../../core/application/usecases/check-grpcs.usecase';

const mockGrpcHealhIndicator = {
  checkService: jest.fn().mockImplementation((checkGrpc: CheckGrpc) =>
    Promise.resolve({
      [checkGrpc.name]: {
        status: 'up',
      },
    }),
  ),
};

const checkGrpcs: CheckGrpc[] = [
  {
    name: 'firstGrpc',
    url: 'grpc1.url',
    port: '5002',
  },
  {
    name: 'secondGrpc',
    url: 'grpc2.url',
    port: '5003',
  },
];

describe('CheckGrpcsUseCase', () => {
  let checkGrpcsUseCase: CheckGrpcsUseCase;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: GRPCHealthIndicator,
          useValue: mockGrpcHealhIndicator,
        },
        CheckGrpcsUseCase,
      ],
    }).compile();

    checkGrpcsUseCase = module.get<CheckGrpcsUseCase>(CheckGrpcsUseCase);
  });

  it('should be defined', () => {
    expect(checkGrpcsUseCase).toBeDefined();
  });

  it('should return results as HealthIndicatorFunctions', () => {
    const results: HealthIndicatorFunction[] =
      checkGrpcsUseCase.results(checkGrpcs);
    expect(results.length).toBe(2);
    results.forEach((result: HealthIndicatorFunction) => {
      expect(result()).toBeInstanceOf(Promise);
    });
  });

  it('should return an empty results array if no grpcs are given', () => {
    const emptyResults: HealthIndicatorFunction[] = checkGrpcsUseCase.results(
      [],
    );
    expect(emptyResults.length).toBe(0);
    const emptyFunctionArray: HealthIndicatorFunction[] =
      checkGrpcsUseCase.results([]);
    expect(emptyFunctionArray).toHaveLength(0);
  });
});
