import { Test, TestingModule } from '@nestjs/testing';
import { CheckMicroservice } from '../../core/domain/types/health.types';
import {
  HealthIndicatorFunction,
  MicroserviceHealthIndicator,
} from '@nestjs/terminus';
import { Transport } from '@nestjs/microservices';
import { CheckMicroservicesUseCase } from '../../core/application/usecases/check-microservices.usecase';

const mockMicroservicesHealhIndicator = {
  pingCheck: jest
    .fn()
    .mockImplementation((checkMicroservice: CheckMicroservice) =>
      Promise.resolve({
        [checkMicroservice.name]: {
          status: 'up',
        },
      }),
    ),
};

const checkMicroservices: CheckMicroservice[] = [
  {
    name: 'firstMicroservice',
    transport: Transport.TCP,
    host: 'microservice1.host',
    port: '5001',
  },
  {
    name: 'secondMicroservice',
    transport: Transport.REDIS,
    host: 'microservice2.host',
    port: '6379',
  },
];

describe('CheckMicroservicesUseCase', () => {
  let checkMicroservicesUseCase: CheckMicroservicesUseCase;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: MicroserviceHealthIndicator,
          useValue: mockMicroservicesHealhIndicator,
        },
        CheckMicroservicesUseCase,
      ],
    }).compile();

    checkMicroservicesUseCase = module.get<CheckMicroservicesUseCase>(
      CheckMicroservicesUseCase,
    );
  });

  it('should be defined', () => {
    expect(checkMicroservicesUseCase).toBeDefined();
  });

  it('should return results as HealthIndicatorFunctions', () => {
    const results: HealthIndicatorFunction[] =
      checkMicroservicesUseCase.results(checkMicroservices);
    expect(results.length).toBe(2);
    results.forEach((result: HealthIndicatorFunction) => {
      expect(result()).toBeInstanceOf(Promise);
    });
  });

  it('should return an empty results array if no microservices are given', () => {
    const emptyResults: HealthIndicatorFunction[] =
      checkMicroservicesUseCase.results([]);
    expect(emptyResults.length).toBe(0);
    const emptyFunctionArray: HealthIndicatorFunction[] =
      checkMicroservicesUseCase.results([]);
    expect(emptyFunctionArray).toHaveLength(0);
  });
});
