import { DynamicModule, Module, Provider } from '@nestjs/common';
import {
  CheckGrpc,
  CheckMicroservice,
  CheckRepository,
  CheckUrl,
  HealthModuleAsyncOptions,
  HealthModuleOptions,
} from './core/domain/types/health.types';
import { TerminusModule } from '@nestjs/terminus';
import { HealthGrpcController } from './interface/grpc-controllers/health.grpc.controller';
import { HealthHttpController } from './interface/http-controllers/health.http.controller';
import {
  CHECK_REPOSITORIES,
  CRITICAL_LOGGING_KEY,
  HEALTH_OPTIONS,
  HEALTH_MESSAGE_PUBLISHER,
  CHECK_URLS,
  CHECK_MICROSERVICES,
  CHECK_GRPCS,
} from './health.di-tokens';
import { MessagePublisherPort } from '@mobicoop/ddd-library';
import { RepositoryHealthIndicator } from './core/application/health-indicators/repository.health-indicator';
import { CheckRepositoriesUseCase } from './core/application/usecases/check-repositories.usecase';
import { CheckUrlsUseCase } from './core/application/usecases/check-urls.usecase';
import { HttpModule } from '@nestjs/axios';
import { CheckMicroservicesUseCase } from './core/application/usecases/check-microservices.usecase';
import { CheckGrpcsUseCase } from './core/application/usecases/check-grpcs.usecase';

export const createAsyncOptionsProvider = (
  options: HealthModuleAsyncOptions,
): Provider => ({
  provide: HEALTH_OPTIONS,
  useFactory: options.useFactory,
  inject: options.inject,
});

const grpcControllers = [HealthGrpcController];

const httpControllers = [HealthHttpController];

const healthIndicators: Provider[] = [RepositoryHealthIndicator];

const useCases: Provider[] = [
  CheckRepositoriesUseCase,
  CheckUrlsUseCase,
  CheckMicroservicesUseCase,
  CheckGrpcsUseCase,
];

@Module({})
export class HealthModule {
  static forRootAsync(
    options: HealthModuleAsyncOptions,
    isGlobal = true,
  ): DynamicModule {
    const asyncOptionsProvider: Provider = createAsyncOptionsProvider(options);

    return {
      global: isGlobal,
      module: HealthModule,
      imports: [
        TerminusModule,
        HttpModule,
        ...(options.imports as DynamicModule[]),
      ],
      controllers: [...grpcControllers, ...httpControllers],
      providers: [
        asyncOptionsProvider,
        ...healthIndicators,
        ...useCases,
        {
          provide: HEALTH_MESSAGE_PUBLISHER,
          useFactory: async (
            options: HealthModuleOptions,
          ): Promise<MessagePublisherPort> =>
            options.messagePublisher as MessagePublisherPort,
          inject: [HEALTH_OPTIONS],
        },
        {
          provide: CHECK_REPOSITORIES,
          useFactory: async (
            options: HealthModuleOptions,
          ): Promise<CheckRepository[]> =>
            options.checkRepositories as CheckRepository[],
          inject: [HEALTH_OPTIONS],
        },
        {
          provide: CHECK_URLS,
          useFactory: async (
            options: HealthModuleOptions,
          ): Promise<CheckUrl[]> => options.checkUrls as CheckUrl[],
          inject: [HEALTH_OPTIONS],
        },
        {
          provide: CHECK_MICROSERVICES,
          useFactory: async (
            options: HealthModuleOptions,
          ): Promise<CheckMicroservice[]> =>
            options.checkMicroservices as CheckMicroservice[],
          inject: [HEALTH_OPTIONS],
        },
        {
          provide: CHECK_GRPCS,
          useFactory: async (
            options: HealthModuleOptions,
          ): Promise<CheckGrpc[]> => options.checkGrpcs as CheckGrpc[],
          inject: [HEALTH_OPTIONS],
        },
        {
          provide: CRITICAL_LOGGING_KEY,
          useFactory: async (options: HealthModuleOptions): Promise<string> =>
            options.criticalLoggingKey,
          inject: [HEALTH_OPTIONS],
        },
      ],
      exports: [asyncOptionsProvider, HttpModule],
    };
  }
}
