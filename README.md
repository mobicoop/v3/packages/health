# Mobicoop V3 - Health package

Health module NPM package for Mobicoop V3 services.

## Installation

```bash
npm install --save @mobicoop/health-module
```
